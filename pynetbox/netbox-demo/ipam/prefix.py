#!/usr/bin/env python3
#
# prefix.py
#
# mgoncalves@golesuite.com - Sun 14 Jun 2020 12:40:13 AM -04
#
# demo code to exemplify the use of pynetbox

# nao estamos trabalhando com classes para facilitar o entendimento dos alunos

import pynetbox
import os

VMS_NUMBER = int(os.getenv('VMS_NUMBER', '10'))
VMS_NAME = os.getenv('VMS_NAME', 'devopsbase')

nb = pynetbox.api(
    'https://netbox-demo.blacktourmaline.com.br',
    token=' e74f4af1cff5b442b762cac693931df27678cea3'
    #private_key_file='/path/to/private-key.pem',
)

devops_site = {
    'slug': 'comp-a-001',
}

devops_tenant = {
    'slug': 'training',
}

site = nb.dcim.sites.get(slug=devops_site['slug'])
tt = nb.tenancy.tenants.get(slug=devops_tenant['slug'])


#######################################################

devops_prefix = {
    'tags': ['trainning'],
    'prefix': '172.31.224.0/25',
    'site': site.id,
    'tenant': tt.id
}


prefixes = nb.ipam.prefixes.filter(tags='trainning')
if len(prefixes) != 0:

    for prefix in prefixes:
        print(f'''Removing Prefix: {prefix.prefix}''')
        prefix.delete()
        
else:

    print(f'''Creating prefix: {devops_prefix['prefix']}\n''')    
    nb.ipam.prefixes.create(devops_prefix)
