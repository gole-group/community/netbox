#!/usr/bin/env python3
#
# aggregates.py 
#
# mgoncalves@golesuite.com - Sun 14 Jun 2020 12:40:13 AM -04
#
# demo code to exemplify the use of pynetbox

import pynetbox

nb = pynetbox.api(
    'https://netbox-demo.blacktourmaline.com.br',
    token=' e74f4af1cff5b442b762cac693931df27678cea3'
    #private_key_file='/path/to/private-key.pem',
)

# create rir

devops_rir = {
    'name': 'DEVOPS-BASE',
    'slug': 'devops-base',
    'is_private': True
}
rir = nb.ipam.rirs.get(name=devops_rir['name'])
if rir is None:
    print(f'''Creating rir: {devops_rir['name']}''')
    nb.ipam.rirs.create(devops_rir)
    rir = nb.ipam.rirs.get(name=devops_rir['name'])

devops_aggregate = {
    'description': 'private Classe B',
    'prefix': '172.16.0.0/12',
    'rir': rir.id
}
aggregate = nb.ipam.aggregates.get(prefix=devops_aggregate['prefix'])
if aggregate is None:
    print(f'''Creating aggregate: {devops_aggregate['prefix']}''')
    nb.ipam.aggregates.create(devops_aggregate)
    aggregate = nb.ipam.aggregates.get(prefix=devops_aggregate['prefix'])
else:
    print(f'''Removing aggregate: {devops_aggregate['prefix']}''')
    aggregate.delete()
    
    print(f'''Removing rir: {devops_rir['name']}''')
    rir.delete()
    

