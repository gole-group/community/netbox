#!/usr/bin/env python3
#
# site.py
#
# mgoncalves@golesuite.com - Sun 14 Jun 2020 12:40:13 AM -04
#
# demo code to exemplify the use of pynetbox

# nao estamos trabalhando com classes para facilitar o entendimento dos alunos

import pynetbox

nb = pynetbox.api(
    'https://netbox-demo.blacktourmaline.com.br',
    token=' e74f4af1cff5b442b762cac693931df27678cea3'
    #private_key_file='/path/to/private-key.pem',
)

devops_tenant = {'slug': 'training'}
tt = nb.tenancy.tenants.get(slug=devops_tenant['slug'])

# create tenant group
devops_region = {
    'name': 'Brasil',
    'slug': 'brasil',
}
region = nb.dcim.regions.get(slug=devops_region['slug'])
if region is None:
    print(f'''Creating Region: {devops_region['name']}''')
    nb.dcim.regions.create(devops_region)
    region = nb.dcim.regions.get(slug=devops_region['slug'])

devops_site = {
    'name': 'COMP-A-001',
    'slug': 'comp-a-001',
    'region': region.id,
    'tenant': tt.id,
    'time_zone': 'America/Sao_Paulo',    
    'description': 'Company A DC Matriz',
    'physical_address': 'Rodovia PE09, Granja Sso Judas Tadeu, Porto de Galinhas, BR 55592-000',
    'contact_name': 'Jose Bonifacio',
    'contact_phone': '+551139399339',
    'contact_email': 'jose.bonifacio@brasil.gov'
}

site = nb.dcim.sites.get(slug=devops_site['slug'])
if site is None:
    print(f'''Creating site: {devops_site['name']}''')
    nb.dcim.sites.create(devops_site)
    site = nb.dcim.sites.get(slug=devops_tenant['slug'])
else:
    print(f'''Removing sites: {devops_site['slug']}''')
    site.delete()
  
    print(f'''Removing Region: {devops_region['name']}''')
    region.delete()
    

