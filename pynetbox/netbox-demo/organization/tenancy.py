#!/usr/bin/env python3
#
# tenancy.py
#
# mgoncalves@golesuite.com - Sun 14 Jun 2020 12:40:13 AM -04
#
# demo code to exemplify the use of pynetbox

import pynetbox

nb = pynetbox.api(
    'https://netbox-demo.blacktourmaline.com.br',
    token=' e74f4af1cff5b442b762cac693931df27678cea3'
    #private_key_file='/path/to/private-key.pem',
)

# create tenant group
devops_tenant_group = {
    'name': 'COMPANY A',
    'slug': 'company-a',
}
ttg = nb.tenancy.tenant_groups.get(slug=devops_tenant_group['slug'])
if ttg is None:
    print(f'''Creating Tenant group: {devops_tenant_group['name']}''')
    nb.tenancy.tenant_groups.create(devops_tenant_group)
    ttg = nb.tenancy.tenant_groups.get(slug=devops_tenant_group['slug'])

devops_tenant = {
    'name': 'Training',
    'slug': 'training',
    'description': 'Company A Training Area',
    'group': ttg.id
}
tt = nb.tenancy.tenants.get(slug=devops_tenant['slug'])
if tt is None:
    print(f'''Creating Tenant: {devops_tenant['name']}''')
    nb.tenancy.tenants.create(devops_tenant)
    tt = nb.tenancy.tenants.get(slug=devops_tenant['slug'])
else:
    print(f'''Removing Tenant: {devops_tenant['slug']}''')
    tt.delete()
    
    print(f'''Removing Tenant Group: {devops_tenant_group['name']}''')
    ttg.delete()
    

