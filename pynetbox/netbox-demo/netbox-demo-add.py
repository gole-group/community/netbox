#!/usr/bin/env python3
#
# netbox-demo
# mgoncalves@golesuite.com - Sun 14 Jun 2020 12:40:13 AM -04
#
# demo code to exemplify the use of pynetbox

# create aggregates
from ipam import aggregates

# create tenancy
from organization import tenancy

# create site
from dcim import site

# virtualization
from virtualization import virtual

# add prefix
from ipam import prefix

