#!/usr/bin/env python3
#
# netbox-demo-del.py
# mgoncalves@golesuite.com - Sun 14 Jun 2020 12:40:13 AM -04
#
# demo code to exemplify the use of pynetbox

# delete prefix
from ipam import prefix

# delete virtualization
from virtualization import virtual

# delete site
from dcim import site

# delete tenancy
from organization import tenancy


# delete aggregates
from ipam import aggregates


