#!/usr/bin/env python3
#
# virtual.py
#
# mgoncalves@golesuite.com - Sun 14 Jun 2020 12:40:13 AM -04
#
# demo code to exemplify the use of pynetbox

# nao estamos trabalhando com classes para facilitar o entendimento dos alunos

import pynetbox
import os
import time

VMS_NUMBER = int(os.getenv('VMS_NUMBER', '20'))
VMS_NAME = os.getenv('VMS_NAME', 'devopsbase')

nb = pynetbox.api(
    'https://netbox-demo.blacktourmaline.com.br',
    token=' e74f4af1cff5b442b762cac693931df27678cea3'
    #private_key_file='/path/to/private-key.pem',
)


devops_site = {
    'slug': 'comp-a-001',
}

devops_tenant = {
    'slug': 'training',
}

site = nb.dcim.sites.get(slug=devops_site['slug'])
tt = nb.tenancy.tenants.get(slug=devops_tenant['slug'])


#######################################################

# create cluster group
devops_cluster_group = {
    'name': 'Cluster Trainning',
    'slug': 'cluster-trainning',
}
cg = nb.virtualization.cluster_groups.get(slug=devops_cluster_group['slug'])


# create cluster type
devops_cluster_type = {
    'name': 'PROXMOX',
    'slug': 'proxmox',
}
ct = nb.virtualization.cluster_types.get(slug=devops_cluster_type['slug'])


devops_cluster = {
    'name': 'Trainning 01',
    'slug': 'trainning-01',
    'tenant': tt.id,
    'site': site.id,
    'comments': 'Trainning Cluster 01'
}    

vms = nb.virtualization.virtual_machines.filter(tags='trainning')
if len(vms) != 0:

    cluster = nb.virtualization.clusters.get(name=devops_cluster['name'])    
    for vm in vms:
        print(f'''Removing VM: {vm.name}''')
        vm.delete()


    print(f'''Removing Cluster: {devops_cluster['name']}''')
    cluster.delete()
    
    print(f'''Removing Cluster type: {devops_cluster_type['slug']}''')
    ct.delete()
  
    print(f'''Removing Cluster Group: {devops_cluster_group['name']}''')
    cg.delete()
    
else:

    print(f'''Creating Cluster Group: {devops_cluster_group['name']}\n''')
    #nb.virtualization.cluster_groups.create(devops_cluster_group)
    cg = nb.virtualization.cluster_groups.get(slug=devops_cluster_group['slug'])
    devops_cluster['group'] = cg.id    
    
    print(f'''Creating Cluster Type: {devops_cluster_type['name']}\n''')

    #nb.virtualization.cluster_types.create(devops_cluster_type)
    ct = nb.virtualization.cluster_types.get(slug=devops_cluster_type['slug'])
    devops_cluster['type'] = ct.id    

    print(f'''Creating cluster: {devops_cluster['name']}\n''')
    #nb.virtualization.clusters.create(devops_cluster)
    cluster = nb.virtualization.clusters.get(name=devops_cluster['name'])

    vms_list = []

    # pfsense
    vm_obj = {
        'name': 'devopsgw',
        'cluster': cluster.id,
        'site': site.id,
        'tenant': tt.id,
        'vcpus': 4,
        'memory': 3072,
        'disk': 8,
        'tags': [ 'trainning', 'gw'],
        'comments': 'Gw VM for trainning',
        'local_context_data': {
            'priv_user': 'root',
        }
    }
    vms_list.append(vm_obj)

    # nginx mgt
    vm_obj = {
        'name': 'devopsmgt',
        'cluster': cluster.id,
        'site': site.id,
        'tenant': tt.id,
        'vcpus': 4,
        'memory': 3072,
        'disk': 8,
        'tags': [ 'trainning', 'mgt'],
        'comments': 'VM for trainning',
        'local_context_data': {
            'priv_user': 'bt',
        }
    }
    vms_list.append(vm_obj)
    
    VM_LOOP = 1
    while VM_LOOP <= VMS_NUMBER:
        VM = f'{VMS_NAME}{str(VM_LOOP)}'
        VM_SSH = 12000 + VM_LOOP
        VM_HTTP = 12010 + VM_LOOP

        vm_obj = {
            'name': VM,
            'cluster': cluster.id,
            'site': site.id,
            'tenant': tt.id,
            'vcpus': 4,
            'memory': 3072,
            'disk': 8,
            'comments': 'VMs for trainning',
            'tags': [ 'trainning', 'clients' ],
            'local_context_data': {
                'external_ip': '200.201.195.116',
                'external_ssh_port': str(VM_SSH),
                'external_http_port': str(VM_HTTP),
                'priv_user': 'bt',
                'private_cert': 'msg pessoal em conf.golesuite.com'
            }
        }

        vms_list.append(vm_obj)
        VM_LOOP += 1

        
    trainning_prefix = nb.ipam.prefixes.filter(tags='trainning')
    trainning_prefix = trainning_prefix[0]
        
    for vm_obj in vms_list:
        print(f'''Creating virtual machine: {vm_obj['name']}''')
        vm_new = nb.virtualization.virtual_machines.create(vm_obj)

        # add interface

        vm_int = {
            'virtual_machine': vm_new.id,
            'name': 'eth0',
        }
        vm_int = nb.virtualization.interfaces.create(vm_int)
        trainning_prefix.available_ips.create({'interface': vm_int.id})
        time.sleep(1)
